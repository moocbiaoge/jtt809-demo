package com.jtt809.demo.down.codec;

import com.jtt809.demo.down.pojo.ResponseClientFactory;
import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import com.jtt809.demo.up.pojo.Response;
import com.jtt809.demo.up.util.HexBytesUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * jtt809解码类
 */
@Slf4j
public class ClientDecoderJtt809 extends ByteToMessageDecoder {

    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        try {
            short startByte = in.getByte(0);
            //粘包后导致的半包数据不要读
            if (BasePackage.MSG_HEAD_FLAG != startByte) {
                return;
            }

            // region 报文转义
            ByteBuf tempByteBuf = Unpooled.copiedBuffer(in);
            ByteBuf finalBytebuf = Unpooled.buffer();
            BasePackage.messageInversion(tempByteBuf.array(), finalBytebuf);

            ReferenceCountUtil.release(tempByteBuf);
            tempByteBuf = Unpooled.copiedBuffer(finalBytebuf);
            byte[] bytesTemp = tempByteBuf.array();
            ReferenceCountUtil.release(tempByteBuf);
            log.info("======================> 上级平台报文转义 : {}", HexBytesUtil.bytesToHex(bytesTemp));
            // end region

            //数据体解密
            short encryptFlag = finalBytebuf.getByte(ConstantJtt809.ENCRYPT_FLAG_INDEX_OF_PACKAGE);
            if (ConstantJtt809.ENCRYPT_FLAG_PACKAGE_Y == encryptFlag) {
                finalBytebuf = BasePackage.decrypt(finalBytebuf);
            }

            try {
                // 工厂根据传入的指令实例化对象
                Response response = ResponseClientFactory.createResponse(finalBytebuf);
                if (null != response) {
                    out.add(response);
                }
            } finally {
                ReferenceCountUtil.release(finalBytebuf);
                in.clear();
            }
        } catch (Exception e) {
            log.error("===========> 下级平台解码异常", e);
        }
    }
}
