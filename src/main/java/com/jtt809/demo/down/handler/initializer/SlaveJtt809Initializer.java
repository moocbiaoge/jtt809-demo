package com.jtt809.demo.down.handler.initializer;

import com.jtt809.demo.down.codec.ClientDecoderJtt809;
import com.jtt809.demo.down.codec.ClinetEncoderJtt809;
import com.jtt809.demo.down.handler.SlaveLinkClientJtt809Handler;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

public class SlaveJtt809Initializer extends ChannelInitializer<SocketChannel> {

    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline channelPipeline = ch.pipeline();

        //粘包分隔符
        final ByteBuf delimiter = Unpooled.buffer(1);
        delimiter.writeByte(BasePackage.MSG_END_FLAG);
        channelPipeline.addLast("delimiter", new DelimiterBasedFrameDecoder(1024, delimiter));


        // 打印日志信息
//        channelPipeline.addLast("loging", new LoggingHandler(LogLevel.INFO));

        // 解码 和 编码
        channelPipeline.addLast("decoder", new ClientDecoderJtt809());
        channelPipeline.addLast("encoder", new ClinetEncoderJtt809());

        // 心跳检测，每隔150s检测一次是否要读事件，如果超过150s你没有读事件的发生，则执行相应的操作
        channelPipeline.addLast("timeout", new IdleStateHandler(150, 0, 0, TimeUnit.SECONDS));

        // 业务逻辑Handler
        channelPipeline.addLast("handler", new SlaveLinkClientJtt809Handler());
    }

}
