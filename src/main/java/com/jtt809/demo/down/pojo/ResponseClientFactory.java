package com.jtt809.demo.down.pojo;

import com.jtt809.demo.down.pojo.command.response.*;
import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import com.jtt809.demo.up.pojo.Response;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * 响应类创建工厂
 */
@Slf4j
public class ResponseClientFactory {

    private static Map<Integer, Class<? extends Response>> responseMap = new HashMap<Integer, Class<? extends Response>>();

    /**
     * 业务数据类型与bean对应的map集合
     */
    static {
        responseMap.put(ConstantJtt809.UP_CONNECT_REP, ResponseClientJtt809_0x1002.class);
        responseMap.put(ConstantJtt809.UP_LINKTEST_RSP, ResponseClientJtt809_0x1006.class);
        responseMap.put(ConstantJtt809.DOWN_CONNECT_REQ, ResponseClientJtt809_0x9001.class);
        responseMap.put(ConstantJtt809.DOWN_LINKTEST_REQ, ResponseClientJtt809_0x9005.class);
        responseMap.put(ConstantJtt809.DOWN_TOTAL_RECY_BACK_MSG, ResponseClientJtt809_0x9101.class);
    }

    /**
     * 创建响应数据包
     *
     * @param buf   数据包
     * @return
     */
    public static Response createResponse(ByteBuf buf) {
        int msgId = buf.getUnsignedShort(ConstantJtt809.MSG_ID_INDEX_OF_PACKAGE);

        // 根据业务获取响应类
        Class<? extends Response> cls = null;
        int dataType = 0;
        switch (msgId) {
            // 主链路动态信息交换消息
            case ConstantJtt809.UP_EXG_MSG:
                // 主链路静态信息交换消息
            case ConstantJtt809.UP_BASE_MSG:
                // 获取子业务类型 data_type在数据包中的位置
                dataType = buf.getUnsignedShort(BasePackage.isJtt809Version2019() ? ConstantJtt809.DATA_TYPE_INDEX_OF_PACKAGE_2019 : ConstantJtt809.DATA_TYPE_INDEX_OF_PACKAGE_2011);
                cls = responseMap.get(dataType);
                break;
            default:
                cls = responseMap.get(msgId);
        }

        if (cls == null) {
            log.error("=========>【下级平台】不支持的应答类型：0x{}", dataType > 0 ? Integer.toHexString(dataType) : Integer.toHexString(msgId));
            return null;
        }

        try {
            Response response = cls.newInstance();
            response.decode(buf);

            return response;
        } catch (Exception e) {
            log.error("=========>【上级平台】创建响应数据包异常：{}", e.getMessage());
        }
        return null;
    }

}
