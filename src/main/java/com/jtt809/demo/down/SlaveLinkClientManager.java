package com.jtt809.demo.down;

import com.jtt809.demo.down.handler.initializer.SlaveJtt809Initializer;
import com.jtt809.demo.up.config.Jtt809Config;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 从链路
 * 下级平台服务端
 */
@Data
@Slf4j
public class SlaveLinkClientManager implements Runnable{

    /**
     * 绑定IP
     */
    private String ip;

    /**
     * 绑定端口
     */
    private int port;

    public SlaveLinkClientManager(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void run() {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    // 保持长连接
                    .childOption(ChannelOption.SO_KEEPALIVE,true)
                    // 标识当服务器请求处理线程全满时，用于临时存放已完成三次握手的请求的队列的最大长度
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    .childHandler(new SlaveJtt809Initializer());

            // 绑定端口，同步等待成功
            ChannelFuture channelFuture = serverBootstrap.bind(this.ip, this.port).sync();
            log.info("\n\n\n==========> {}版本下级平台服务端启动成功。绑定IP：{}，绑定端口：{}\n\n\n", Jtt809Config.JTT809_VERSION, this.ip, this.port);

            // 等待服务器监听端口关闭
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            log.info("==========> 下级平台服务端启动出错。错误：{}", e.getMessage());
        } finally {
            // 释放线程池资源
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
