package com.jtt809.demo.up.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Configurable
@Data
public class Jtt809Config {

    /**
     * jtt809协议版本
     */
    public static int JTT809_VERSION;

    /**
     * 对接方来源：1市交运局(jtt809-2019)、2市园林环卫(jtt809-2011)、3香洲区环卫(jtt809-2011)
     */
    public static int JTT809_SOURCE;

    /**
     * 是否在主链路登录成功后创建从链路
     */
    public static boolean JTT809_SLAVE_CREATE;

    /**
     * 上级平台服务是否启动
     */
    public static boolean JTT809_NETTY_SERVER_UP_OPEN;

    /**
     * 上级平台服务地址
     */
    public static String JTT809_NETTY_SERVER_UP_IP;

    /**
     * 上级平台服务端口
     */
    public static int JTT809_NETTY_SERVER_UP_PORT;

    /**
     * 下级平台服务是否启动
     */
    public static boolean JTT809_NETTY_SERVER_DOWN_OPEN;

    /**
     * 下级平台发送测试数据是否启动
     */
    public static boolean JTT809_NETTY_SERVER_DOWN_TEST;

    /**
     * 下级平台服务地址
     */
    public static String JTT809_NETTY_SERVER_DOWN_IP;

    /**
     * 下级平台服务端口
     */
    public static int JTT809_NETTY_SERVER_DOWN_PORT;

    /**
     * 下级平台平台账号登录帐号
     */
    public static int JTT809_NETTY_SERVER_DOWN_USERID;

    /**
     * 下级平台登录密码
     */
    public static String JTT809_NETTY_SERVER_DOWN_PASSWORD;

    /**
     * 下级平台接入码，上级平台给下级平台分配唯一标识码。
     */
    public static int JTT809_FACTORY_ACCESS_CODE;

    /**
     * 下级平台唯一编码（主链路登录应答校验码）
     */
    public static int JTT809_FACTORY_ACCESS_ID;

    /**
     * 收到x条下级平台发送的车辆定位信息后通知下级平台
     */
    public static int JTT809_RECEIVE_GPS_OVERFLOW_NOTICE_COUNT;

    /**
     * 加密参数M1
     */
    public static  long JTT809_NETTY_ENCRYPT_M1;

    /**
     * 加密参数IA1
     */
    public static  long JTT809_NETTY_ENCRYPT_IA1;

    /**
     * 加密参数IC1
     */
    public static  long JTT809_NETTY_ENCRYPT_IC1;

    /**
     * 秘钥
     */
    public static  long JTT809_NETTY_ENCRYPT_KEY;

    @Value("${jtt809.version}")
    private int jtt809Version;

    @Value("${jtt809.source}")
    private int jtt809Source;

    @Value("${jtt809.slave-create}")
    private boolean jtt809SlaveCreate;

    @Value("${jtt809.netty.server.up.open}")
    private boolean jtt809NettyServerUpOpen;

    @Value("${jtt809.netty.server.up.ip}")
    private String jtt809NettyServerUpIp;

    @Value("${jtt809.netty.server.up.port}")
    private int jtt809NettyServerUpPort;

    @Value("${jtt809.netty.server.down.ip}")
    private String jtt809NettyServerDownIp;

    @Value("${jtt809.netty.server.down.open}")
    private boolean jtt809NettyServerDownOpen;

    @Value("${jtt809.netty.server.down.test}")
    private boolean jtt809NettyServerDownTest;

    @Value("${jtt809.netty.server.down.port}")
    private int jtt809NettyServerDownPort;

    @Value("${jtt809.netty.server.down.userid}")
    private int jtt809NettyServerDownUserid;

    @Value("${jtt809.netty.server.down.password}")
    private String jtt809NettyServerDownPassword;

    @Value("${jtt809.factory.access.code}")
    private int jtt809FactoryAccessCode;

    @Value("${jtt809.factory.access.id}")
    private int jtt809FactoryAccessId;

    @Value("${jtt809.receive.gps.overflow.notice.count}")
    private int jtt809ReceiveGpsOverflowNoticeCount;

    @Value("${jtt809.netty.encrypt.M1}")
    private long jtt809NettyEncryptM1;

    @Value("${jtt809.netty.encrypt.IA1}")
    private long jtt809NettyEncryptIA1;

    @Value("${jtt809.netty.encrypt.IC1}")
    private long jtt809NettyEncryptIC1;

    @Value("${jtt809.netty.encrypt.key}")
    private long jtt809NettyEncryptKey;

    @PostConstruct
    public void postConstruct() {
        JTT809_VERSION = jtt809Version;
        JTT809_SOURCE = jtt809Source;
        JTT809_SLAVE_CREATE = jtt809SlaveCreate;

        JTT809_NETTY_SERVER_UP_OPEN = jtt809NettyServerUpOpen;
        JTT809_NETTY_SERVER_UP_IP = jtt809NettyServerUpIp;
        JTT809_NETTY_SERVER_UP_PORT = jtt809NettyServerUpPort;

        JTT809_NETTY_SERVER_DOWN_OPEN = jtt809NettyServerDownOpen;
        JTT809_NETTY_SERVER_DOWN_TEST = jtt809NettyServerDownTest;
        JTT809_NETTY_SERVER_DOWN_IP = jtt809NettyServerDownIp;
        JTT809_NETTY_SERVER_DOWN_PORT = jtt809NettyServerDownPort;
        JTT809_NETTY_SERVER_DOWN_USERID = jtt809NettyServerDownUserid;
        JTT809_NETTY_SERVER_DOWN_PASSWORD = jtt809NettyServerDownPassword;

        JTT809_FACTORY_ACCESS_CODE = jtt809FactoryAccessCode;
        JTT809_FACTORY_ACCESS_ID = jtt809FactoryAccessId;
        JTT809_RECEIVE_GPS_OVERFLOW_NOTICE_COUNT = jtt809ReceiveGpsOverflowNoticeCount;

        JTT809_NETTY_ENCRYPT_M1 = jtt809NettyEncryptM1;
        JTT809_NETTY_ENCRYPT_IA1 = jtt809NettyEncryptIA1;
        JTT809_NETTY_ENCRYPT_IC1 = jtt809NettyEncryptIC1;
        JTT809_NETTY_ENCRYPT_KEY = jtt809NettyEncryptKey;
    }

}
