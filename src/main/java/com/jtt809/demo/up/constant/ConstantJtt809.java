package com.jtt809.demo.up.constant;

import io.netty.channel.ChannelHandlerContext;

import java.util.HashMap;
import java.util.Map;

public interface ConstantJtt809 {

    /**
     * jtt809协议版本---2019
     */
    int JTT809_VERSION_2019 = 2019;

    /**
     * msgId在数据包中的位置
     * 计算方式：
     * Head_flag（1 byte） + MSG_LENGTH（4 byte） + MSG_SN（4 byte）= 9
     */
    int MSG_ID_INDEX_OF_PACKAGE = 9;

    /**
     * data_type在数据包中的位置
     * 计算方式：
     */
    int DATA_TYPE_INDEX_OF_PACKAGE_2011 = 45;
    int DATA_TYPE_INDEX_OF_PACKAGE_2019 = 53;

    /**
     * encrypt_flag在数据包中的位置
     * 计算方式：
     */
    int ENCRYPT_FLAG_INDEX_OF_PACKAGE = 18;
    /**
     * 已加密
     */
    short ENCRYPT_FLAG_PACKAGE_Y = 1;

    /**
     * 上下级平台地址关联集合
     */
    Map<String, String> UP_DOWN_PLATFORM_LINK = new HashMap<String, String>(10);

    /**
     * 上级平台地址连接集合
     */
    Map<String, ChannelHandlerContext> UP_PLATFORM = new HashMap<String, ChannelHandlerContext>(10);

    /**
     * 下级平台地址连接集合
     */
    Map<String, ChannelHandlerContext> DOWN_PLATFORM = new HashMap<String, ChannelHandlerContext>(10);

    /**
     * 下级平台是否需要加密数据关联集合
     */
    Map<String, Short> UP_DOWN_PLATFORM_ENCRYPT_FLAG = new HashMap<String, Short>(10);

    /**
     * 链路类型：主链路
     * 描述：主链路登录请求消息
     */
    int UP_CONNECT_REQ = 0x1001;

    /**
     * 链路类型：主链路
     * 描述：主链路登录应答消息
     */
    int UP_CONNECT_REP = 0x1002;

    /**
     * 链路类型：主链路
     * 描述：主链路连接保持请求消息
     */
    int UP_LINKTEST_REQ = 0x1005;

    /**
     * 链路类型：主链路
     * 描述：主链路连接保持应答消息
     */
    int UP_LINKTEST_RSP = 0x1006;

    /**
     * 链路类型：从链路
     * 描述：从链路连接保持请求消息
     */
    int DOWN_LINKTEST_REQ = 0x9005;

    /**
     * 链路类型：从链路
     * 描述：从链路连接保持应答消息
     */
    int DOWN_LINKTEST_RSP = 0x9006;

    /**
     * 链路类型：从链路
     * 描述：从链路连接请求消息
     */
    int DOWN_CONNECT_REQ = 0x9001;

    /**
     * 链路类型：从链路
     * 描述：从链路连接应答消息
     */
    int DOWN_CONNECT_RSP = 0x9002;

    /**
     * 链路类型：主链路
     * 描述：主链路动态信息交换消息
     */
    int UP_EXG_MSG = 0x1200;

    /**
     * 链路类型：主链路
     * 描述：上传车辆注册信息
     */
    int UP_EXG_MSG_REGISTER = 0x1201;

    /**
     * 链路类型：主链路
     * 描述：实时上传车辆定位信息
     */
    int UP_EXG_MSG_REAL_LOCATION = 0x1202;

    /**
     * 链路类型：主链路
     * 描述：车辆定位信息自动补报
     */
    int UP_EXG_MSG_HISTORY_LOCATION = 0x1203;

    /**
     * 链路类型：主链路
     * 描述：上报车辆驾驶员身份识别信息应答
     */
    int UP_EXG_MSG_REPORT_DRIVER_INFO_ACK = 0x120A;

    /**
     * 链路类型：从链路
     * 描述：上报车辆驾驶员身份识别信息请求
     */
    int DOWN_EXG_MSG_REPORT_DRIVER_INFO = 0x920A;

    /**
     * 链路类型：从链路
     * 描述：接收定位信息数量通知消息
     */
    int DOWN_TOTAL_RECY_BACK_MSG = 0x9101;


    /**
     * 链路类型：主链路
     * 描述：主链路静态信息交换消息
     */
    int UP_BASE_MSG = 0x1600;

    /**
     * 链路类型：主链路
     * 描述：补发车辆静态信息应答
     */
    int UP_BASE_MSG_VEHICLE_ADDED_ACK = 0x1601;

    /**
     * 链路类型：从链路
     * 描述：从链路静态信息交换消息
     */
    int DOWN_BASE_MSG = 0x9600;

    /**
     * 链路类型：从链路
     * 描述：补发车辆静态信息请求
     */
    int DOWN_BASE_MSG_VEHICLE_ADDED = 0x9601;

}
