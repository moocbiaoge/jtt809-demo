package com.jtt809.demo.up.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;

/**
 *  主链路连接保持应答消息
 *  链路类型:主链路。
 *  消息方向:I 级平台往下级平台。
 *  业务数据类型标识:UP_ LINKTEST_ RSP。
 *  描述:上级平台收到下级平台的主链路连接保持请求消息后，向下级平台返回.主链路连
 *  接保持应答消息，保持主链路的连接状态。
 *  主链路连接保持应答消息,数据体为空。
 */
public class RequestJtt809_0x1006 extends BasePackage {

    public RequestJtt809_0x1006() {
        super(ConstantJtt809.UP_LINKTEST_RSP);
        this.msgBodyLength = 0;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {

    }
}
