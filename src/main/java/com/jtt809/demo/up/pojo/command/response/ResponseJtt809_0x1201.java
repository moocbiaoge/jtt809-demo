package com.jtt809.demo.up.pojo.command.response;

import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import lombok.Data;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 上传车辆注册信息
 * 子业务类型标识：UP_EXG_MSG_REGISTER
 * 描述：上传车辆注册信息
 */
@Data
public class ResponseJtt809_0x1201 extends ResponseJtt809_0x1200_VehiclePackage {

    /**
     * 平台唯一编号
     * 11 byte
     */
    private String platformId;

    /**
     * 车载终端厂商唯一编号
     * 11 byte
     */
    private String producerId;

    /**
     * 车载终端型号，不足20位时以“\0”终结
     * 20 byte
     */
    private String terminalModeType;

    /**
     * 车载终端通讯模块IMEI码
     * 15 byte
     */
    private String imeiId;

    /**
     * 车载终端编号，大写字母和数字组成
     * 7 byte
     */
    private String terminalId;

    /**
     * 车载终端SIM卡电话号码。号码不足12位，则在前补充数字0
     * 12 byte
     */
    private String terminalSimCode;

    @Override
    protected void decodeDataImpl(ByteBuf buf) {
        this.platformId = buf.readBytes(11).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
        this.producerId = buf.readBytes(11).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
        if (isJtt809Version2019()) {
            this.terminalModeType = buf.readBytes(30).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
            this.imeiId = buf.readBytes(15).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
            this.terminalId = buf.readBytes(30).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
            this.terminalSimCode = buf.readBytes(13).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
        } else {
            this.terminalModeType = buf.readBytes(20).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
            this.terminalId = buf.readBytes(7).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
            this.terminalSimCode = buf.readBytes(12).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
        }
    }
}
