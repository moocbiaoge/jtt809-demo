package com.jtt809.demo.up.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import lombok.Data;

/**
 * 从链路连接请求消息
 * 链路类型:从链路。
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_CONNECT_REQ。
 * 描述:主链路建立连接后，上级平台向下级平台发送从链路连接清求消息，以建立从链路连接
 */
@Data
public class RequestJtt809_0x9001 extends BasePackage {

    private int verifyCode;

    public RequestJtt809_0x9001() {
        super(ConstantJtt809.DOWN_CONNECT_REQ);
        this.msgBodyLength = 4;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        // 4 byte
        buf.writeInt(getVerifyCode());
    }
}
