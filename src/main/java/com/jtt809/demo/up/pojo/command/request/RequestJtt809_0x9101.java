package com.jtt809.demo.up.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import lombok.Data;

/**
 * 接收车辆定位信息数量通知消息
 * 链路类型：从链路。
 * 消息方向:上级平台往下级平台
 * 业务类型标识:	DOWN_TOTAL_RECV_BACK_MSG.
 * 描述:上级平台向下级平台定星通知已经收到下级平台上传的车辆定位信息数量(如:每收到10,000 条车辆定位信息通知一次)，
 * 本条消息不需下级平台应答。
 *
 * 注：采用 UTC 时间表示，如 2010-1-10 9:7:54 的 UTC 值为 1263085674，其在协议中表示为0x000000004B49286A.
 */
@Data
public class RequestJtt809_0x9101 extends BasePackage {

    /**
     * 开始时间-结束时间期间，共收到的车辆定位信息数量
     */
    private int dynamicInfoTotal;

    /**
     * 开始时间，用 UTC 时间表示
     */
    private long startTime;

    /**
     * 结束时间，用 UTC 时间表示
     */
    private long endTime;

    public RequestJtt809_0x9101() {
        super(ConstantJtt809.DOWN_TOTAL_RECY_BACK_MSG);
        this.msgBodyLength = 20;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        // 4 byte
        buf.writeInt(getDynamicInfoTotal());
        // 8 byte
        buf.writeLong(getStartTime());
        // 8 byte
        buf.writeLong(getEndTime());
    }
}
